Running Job
************

UCC2 uses `Slurm`_ to manage all user jobs. 

.. _Slurm: http://www.schedmd.com/slurmdocs/slurm.html


.. note::

    To any Slurm commands, run ``source /u/systems/slurm/default/settings.sh``
    to set the proper environment. This may change when we go into production mode.


.. _queue-structure:

Queue structure
===================

Currently, the available queues are:

===========================  ===============================
Queue name                   Time limit (h:m:s)
===========================  ===============================
normal                       24:00:00
debug                         2:00:00
===========================  ===============================

The *debug* queue, intended for running quick tests, is associated with higher priority than the *normal* queue.  
The *normal* queue is intended for all production runs.

To see available queues, use::

  $ sinfo --summarize

The names under column "PARTITION" are the queues. You may specify different output format; see ``man sinfo``.

.. _slurm-params:

Common Job Parameters
=========================

You need to specify Slurm parameters in your job script, or from the
command line, to request appropriate resources. See
:ref:`job-script-example` about how these parameters are used in real
examples. Some commonly Slurm parameters are listed below. Most of
them can be used (unless logically contradicting ones) together with
others in one job submission.

Time limit, ``-t``

    The maximum wall-clock limit of a job. Once this limit is exceeded, the job
    is terminated.

Number of nodes, ``-N``

    The total number of nodes a job will use.

Number of cores, ``-n``

    The total number of CPU cores a job will use. If not specified,
    it is default to 1.

Exclusive access to a node, ``--exclusive``

    Prevent other jobs from running on the same node.

Email address, ``--mail-user=<email address>``
    
    Your email address must be in the format of
    ``your_userid@mail``. Using a real email address, such as
    ``your_name@yahoo.com`` or ``your_name@google.com`` or
    ``your_name@ucla.edu`` **will not** work.

Email notification, ``--mail-type=<option>``

    Available options include ``BEGIN``, ``END``, ``FAIL``, or
    ``ALL``. Use together with ``--mail-user``.



Interactive Executation
========================

To run a command interactively on a compute node, use:: 

  $ srun <command>


Interactive Session
======================

To allocate and get an interactive session, use::

   $ srun --pty /bin/bash -l

The ``-l`` option makes bash act like a login shell.
Once the command prompt returns, you are placed on one of the compute
nodes. The time limit is the same as the *debug* queue (see below). If
you need a session with longer time (say 4 hours), use::

  $ srun -p normal -t 4:00:00 --pty /bin/bash -l 


.. _batch job submission:

Batch Job Submission
=====================

Job script
-----------

A job script is a shell script describing exactly how your job will be run. 
The job script usually includes  a number of slurm-specific options. See real examples in
:ref:`job-script-example`.


Job submission
---------------

Once you have a job script (e.g. ``foo.sh``), and have selected a
queue to use (see :ref:`queue-structure`), use the following command to submit your job::

  $ sbatch foo.sh


Check Job Status
----------------

Type::

  $ squeue

Kill Jobs
----------

Type::

  $ scancel <job id>


.. _job-script-example:

Job Script Examples
---------------------

Here are a number of complete job script examples. If you use them,
adjust the parameters properly according to your needs.

Serial job
~~~~~~~~~~~~

.. code-block:: bash

   #!/bin/bash
   #SBATCH -o myjob-%j.out    # both stdout and stderr, %j is job ID
   #SBATCH -J job_name        # job name
   #SBATCH --time=08:00:00    # wall-clock time limit
   #SBATCH -p normal          # use this queue

   # load necessary modules   # set up run-time environment
   ./a.out

OpenMP job
~~~~~~~~~~~~~

.. code-block:: bash

   #!/bin/bash
   #SBATCH -o myjob-%j.stdout  # stdout output
   #SBATCH -e myjob-%j.stderr  # stderr output
   #SBATCH -J job_name         # job name
   #SBATCH --time=01:00:00     # time limit
   #SBATCH -n 8                # use this many CPU cores

   # load necessary modules    # set up run-time environment
   export OMP_NUM_THREADS=8    # run with 8 threads
   ./a.out

MPI job 
~~~~~~~~~

Running on two nodes, *each* with 8 MPI processes (16 MPI processes in total):

.. code-block:: bash

   #!/bin/bash
   #SBATCH -o myjob-%j.out     # stdout + stderr output
   #SBATCH -J job_name         # job name
   #SBATCH -p normal           # use this queue
   #SBATCH --time=08:00:00     # time limit
   #SBATCH -N 2                # use 2 nodes (in total)
   #SBATCH -n 16               # use 16 CPU cores (in total)

   # load necessary modules    # e.g. module load openmpi
   mpirun -n 16 ./a.out        # MPI execution (assuming MPI has been built
                               # with slurm support)

