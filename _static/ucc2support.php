


<?php
//
// Name:   ucc2support.php
// Purpose: to display a form and to create a JIRA ticket or an email to accounts
// Written: 16Aug2013 huqy
// Updated: 21Aug2013 huqy -- fields adjusted according to Shao-Ching's suggestion
// Updated: 21Aug2013 huqy -- fields and functions adjusted according to TV and Shirley's suggestion
//

$toemail = 'hpc@ucla.edu';
$toaccounts = 'accounts@idre.ucla.edu';
$to = 'UCLA IDRE HPC<'.$toemail.'>';
$cluster_name = 'Cluster Name: ucc2';

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) { // Handle the form.

    $error = '';

    // validating the fields
    $fromName = (!empty($_POST['fromName'])) ? trim($_POST['fromName']) : NULL;
    $fromName_body = (!empty($_POST['fromName'])) ? "User's Name: ".trim($_POST['fromName']).PHP_EOL : NULL;
    if (isset($_POST['fromEmail']) && filter_var($_POST['fromEmail'], FILTER_VALIDATE_EMAIL)) {
        $fromEmail = trim($_POST['fromEmail']);
    } else {
        $error .= "Please input your valid email address. <br>";
    }
    $fromh2username = (!empty($_POST['fromh2username'])) ? "Cluster Username: ".trim($_POST['fromh2username']).PHP_EOL : NULL;
    $jobIDs = (!empty($_POST['jobIDs'])) ? "Job ID(s): ".trim($_POST['jobIDs']).PHP_EOL : NULL;
    $fullpathfile = (!empty($_POST['fullpathfile'])) ? "Full path of file(s) in question: ".trim($_POST['fullpathfile']).PHP_EOL : NULL;
    $fullpathcmd = (!empty($_POST['fullpathcmd'])) ? "Full path of job script file (.cmd): ".trim($_POST['fullpathcmd']).PHP_EOL : NULL;
    if ( empty($_POST['problemcat']) || $_POST['problemcat']=='Please select one' ) {
        $error .= "Please select of a catelog of your problem. <br>";
    } else {
        if ($_POST['problemcat']=='account (e.g., password, shell, group permissions)') {
            $to = 'UCLA IDRE Accounts<'.$toaccounts.'>';
        }
        $problemcat = "Problem Category: ".trim($_POST['problemcat']).PHP_EOL;
    }
    if ( empty($_POST['accessfilebyhpc']) ) {
        $error .= "Please indicate whether to allow HPC consultants to access your files. <br>";
    } else {
        $accessfilebyhpc = "Allow HPC Consultant to inspect your files: ".trim($_POST['accessfilebyhpc']).PHP_EOL;
    }
    if ( empty($_POST['emailsubject']) ) {
        $error .= "Please input a subject of your problem. <br>";
    } else {
        $emailsubject = trim($_POST['emailsubject']);
    }
    if ( empty($_POST['emailbody']) ) {
        $error .= "Please input the \"Email body\" field. <br>";
    } else {
        $emailbody = trim($_POST['emailbody']);
    }

    // form headers and send out email
    if (empty($error)) {
        $headers = 'From:'.$fromName.' <'. $fromEmail  . ">\r\n";
        $emailbody = $fromName_body.
                     $cluster_name.PHP_EOL.
                     $fromh2username.
                     $jobIDs.
                     $accessfilebyhpc.
                     $fullpathfile.
                     $fullpathcmd.
                     $problemcat.
                     PHP_EOL.
                     $emailbody;
        if ( mail($to, $emailsubject, $emailbody, $headers) ){
            echo '<p style="font-weight: bold; color: #C00">Mail has been sent!</p>';
        } else {
            $error .= "Failed to send the email!<br>";
        }
    }

    // print out error messages if any.
    if (!empty($error)) {
        echo '<h1>Error!</h1> <p style="font-weight: bold; color: #C00">' . $error . ' Please try again.</p>';
    } else {
        $_POST = array();
    }
    
}
    

// Provide the confirmation window when submit the form to prevent the multiple clicks or robotic submission
$sendemailconfirm = " onclick=\"return confirm('By clicking OK, you will send out the email to UCC2 User Support. Do you want to proceed?.');return false;;\"";

?>

<!-- Display the whole form -->
<h2>UCC2 Cluster User Support</h2>


<ul>
<li>Fill the form and click the "Send" button below.</li>
<li>The more specific and detailed information you provide, the easier the problem can be identified and resolved.</li>
<li>Boxes marked with * are required fields.</p>
</ul>
<form action="ucc2support.php" method="post" >

    <p><label style="float:left; width:32%; margin-right:0.5em; padding-top:0.3em; padding-bottom:0.1em; text-align:right; font-weight:bold; vertical-align:middle;">Your Name:</label>
       <input name="fromName" size="25" maxlength="200" value="<?php echo (isset($_POST['fromName'])) ? $_POST['fromName'] : ''; ?>" /> 
    </p>
    <p><label style="float:left; width:32%; margin-right:0.5em; padding-top:0.3em; padding-bottom:0.1em; text-align:right; font-weight:bold; vertical-align:middle;">Your Email:</label>
       <input name="fromEmail" size="25" maxlength="200" value="<?php echo (isset($_POST['fromEmail'])) ? $_POST['fromEmail'] : ''; ?>" /> *
    </p>
    <p><label style="float:left; width:32%; margin-right:0.5em; padding-top:0.3em; padding-bottom:0.1em; text-align:right; font-weight:bold; vertical-align:middle;">Your Cluster User Name:</label>
       <input name="fromh2username" size="25" maxlength="200" value="<?php echo (isset($_POST['fromh2username'])) ? $_POST['fromh2username'] : ''; ?>" />
    </p>
    <p><label style="float:left; width:32%; margin-right:0.5em; padding-top:0.3em; padding-bottom:0.1em; text-align:right; font-weight:bold; vertical-align:middle;">Job ID(s):</label>
       <input name="jobIDs" size="25" maxlength="400" value="<?php echo (isset($_POST['jobIDs'])) ? $_POST['jobIDs'] : ''; ?>" /> (as listed in "squeue")
    </p>
    <p><label style="float:left; width:32%; margin-right:0.5em; padding-top:0.3em; padding-bottom:0.1em; text-align:right; font-weight:bold; vertical-align:middle;">Allow HPC Consultant to inspect your files:</label>
       <input type="radio" name="accessfilebyhpc" value="Yes" checked/>yes &nbsp;<input type="radio" name="accessfilebyhpc" value="No"/>no
    </p>
    <p><label style="float:left; width:32%; margin-right:0.5em; padding-top:0.3em; padding-bottom:0.1em; text-align:right; font-weight:bold; vertical-align:middle;">Full path of file(s) in question:</label>
       <input name="fullpathfile" size="50" maxlength="800" value="<?php echo (isset($_POST['fullpathfile'])) ? $_POST['fullpathfile'] : ''; ?>" />
    </p>
    <p><label style="float:left; width:32%; margin-right:0.5em; padding-top:0.3em; padding-bottom:0.1em; text-align:right; font-weight:bold; vertical-align:middle;">Full path of job script file:</label>
        <input name="fullpathcmd" size="50" maxlength="800" value="<?php echo (isset($_POST['fullpathcmd'])) ? $_POST['fullpathcmd'] : ''; ?>" />
    </p>
    <p><label style="float:left; width:32%; margin-right:0.5em; padding-top:0.3em; padding-bottom:0.1em; text-align:right; font-weight:bold; vertical-align:middle;">Problem Category:</label>
        <select name="problemcat"><option selected="selected">Please select one</option><option>account (e.g., password, shell, group permissions)</option><option>job scheduling</option><option>other (please specify in subject)</option></select> *
    </p>
    <p><label style="float:left; width:32%; margin-right:0.5em; padding-top:0.3em; padding-bottom:0.1em; text-align:right; font-weight:bold; vertical-align:middle;">Subject:</label>
        <input name="emailsubject" size="47" maxlength="400" value="<?php echo (isset($_POST['emailsubject'])) ? $_POST['emailsubject'] : ''; ?>" /> *
    </p>
    <p><label style="float:left; width:32%; margin-right:0.5em; padding-top:0.3em; padding-bottom:0.1em; text-align:right; font-weight:bold; vertical-align:middle;">Problem Description:</label>
        <textarea name="emailbody" cols="54" rows="10"><?php echo (isset($_POST['emailbody'])) ? $_POST['emailbody'] : ''; ?></textarea> *
    </p>
    <p><label style="float:left; width:32%; margin-right:0.5em; padding-top:0.3em; padding-bottom:0.1em; text-align:right; font-weight:bold; vertical-align:middle;"></label>
        <input type="submit" name="submit" value="Send**" <?php echo $sendemailconfirm?>/> <br>
        <label style="float:left; width:32%; margin-right:0.5em; padding-top:0.3em; padding-bottom:0.1em; text-align:right; font-weight:bold; vertical-align:middle;"></label><small>** By clicking the button, the email will be sent to UCC2 User Support.</small>
    </p>
</form>


