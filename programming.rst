***********
Programming
***********

Compilers
==========

The default compiler is Intel Compiler 13.x. There is also GNU
compiler available. 

Using Intel compiler
---------------------

* For C programs, use ``icc``. Type ``man icc`` to see the options.
* For C++ programs, use ``icpc`` Type ``man icpc`` to see the options.
* For Fortran programs, use ``ifort``. Type ``man ifort`` to see the options.

Using GNU compiler
------------------

* For C programs, use ``gcc``. Type ``man gcc`` to see the options.
* For C++ programs, use ``g++``. Type ``man g++`` to see the options.
* For Fortran programs, use ``gfortran``. Type ``man gfortran`` to see the options.


Compiling OpenMP Programs
==========================

`OpenMP`_ is directly supported by the compiler. Compile OpenMP
programs the same way as if you are compiling a sequential code, with
an additional compiler option:

* For Intel compilers, add the ``-openmp`` option. 
* For GNU compilers, add the ``-fopenmp`` option.

.. _OpenMP: http://openmp.org


Compiling MPI Programs
=======================

The default MPI library is `Intel MPI`_. 

.. _Intel MPI: http://software.intel.com/en-us/articles/intel-mpi-library-documentation

You should always using the MPI compiler wrapper(s) to compiler MPI
source codes. MPI compiler wrappers become available when you load the
appropriate modules.

* For C source files, use ``mpicc``.
* For C++ source files, use ``mpicxx``.
* For Fortran source files, use ``mpif90``.

Other than the name difference, use these compiler wrappers the same
way as using a traditional compiler.  You may use these compiler
wrappers in the makefile(s) too.

To see the full path where the compiler wrapper is coming from (for
example, to verify whether you are using the correct one), use the
``which`` command, e.g.::

  $ which mpicc

To see the underlying compiler options of these compiler wrappers, use
the ``-show`` option, e.g.::

  $ mpicc -show


Linking with Libraries
========================

If your programs make calls to external libraries, you need to link
the compiled object files to them in order to build the
executable. You can link statistically or dynamically with libraries
(provided the library supports it). When linking statically, your
executable can run in a stand-alone fashion, but the file size may be
big. When linking dynamically, the paths to these libraries are
required at run time (typically through the environment varialbe
``LD_LIBRARY_PATH``).  If your program links to both static and
dynamic libraries, the same rule applies to the dynamic ones.

If a program is linked dynamically and is run in batch mode, you need
to ensure the required (dynamically linked) library paths are set up
properly in the job script because the batch mode's run time
environment may be different from your current shell.

See also :ref:`using-modules` and :ref:`batch job submission`.

If you are using `Intel MKL`_, the compiler options can be
complicated. We recommend `Intel MKL Link Line Advisor`_.

.. _Intel MKL Link Line Advisor: http://software.intel.com/sites/products/mkl/
.. _Intel MKL: http://software.intel.com/en-us/articles/intel-math-kernel-library-documentation
