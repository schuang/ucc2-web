***************
Overview
***************

Getting An Account
===================

Please use the following web page to apply for an account:

http://hpc.ucla.edu/hoffman2/getting-started/getting-started.php


Access
=======

Text mode
----------

The cluster's IP address is `ucc2.idre.ucla.edu`.

On Mac OS X or GNU/Linux systems, use ssh (secure shell) to log in::

$ ssh user_id@ucc2.idre.ucla.edu

then type your password.

On Windows, options include:

* use `putty`_
* use the ssh add-on/plug for web browsers (e.g. `firessh`_ for Firefox)
* use `cygwin`_

.. _firessh: https://addons.mozilla.org/en-us/firefox/addon/firessh/
.. _putty: http://www.chiark.greenend.org.uk/~sgtatham/putty/

Graphics mode
--------------

On Mac OS X systems, add the ``-Y`` option to ssh::

$ ssh -Y user_id@ucc2.idre.ucla.edu

On GNU/Linux systems, add the ``-X`` option to ssh::

$ ssh -X user_id@ucc2.idre.ucla.edu

On Windows systems, options include:

* use `NX`_ 
* use `cygwin`_ (with the X11 server packages installed)

.. _NX: http://www.nomachine.com
.. _cygwin: http://www.cygwin.com

Login Nodes
===================

Use the login nodes to edit files and submit jobs. The cluster is a
shared resource; please consider others. Do not run any
compute-intensive jobs on the login nodes. Such jobs may be terminated
without notice.


Compute Nodes
===================

The cluster has 200+ compute nodes.  Each compute node consists of two
2.4GHz Intel `E5530`_ processors (8 CPU cores in total per compute
node).  Each compute node has 24GB of random access memory (RAM). 

See also :ref:`tech-summary`.

.. _E5530:  http://ark.intel.com/products/37103/Intel-Xeon-Processor-E5530-8M-Cache-2_40-GHz-5_86-GTs-Intel-QPI


File System
===================

Home Directory
--------------

The home directory ``/u/home/<UC campus>/<user_id>`` (defined by environment
variable ``$HOME``) is visible on all login and compute nodes. Disk
quota for ``$HOME`` is 20 GB.


Global Scratch Directory
------------------------

The global scratch directory is ``/u/ucc2scratch/``, visible on all
compute and login nodes and shared by all users. The environment
variable ``$SCRATCH`` stores the path of your personal scratch
directory. For example, if your account name is ``abcde``,
``$SCRATCH`` is ``/u/ucc2scratch/a/abcde``.  To enter this personal
scratch directory, issue the command ``cd $SCRATCH``.

Global scratch directory is a high performance system intended for
**temporary** storage for running jobs. Files older than 14 days are
purged automatically. Please note that if the scratch directory gets
full, files not older than 14 days could be purged too, in order to
maintain smooth operation of the cluster.

Once you generated data on scratch directory, you should move them to
your own storage device as soon as possible.  See also
:ref:`data-transfer` about how to transfer files to another site.


Local Scratch Directory
-----------------------

The local scratch directory is ``/work``, visible *only* on a compute
node.


.. _using-modules:

Modules
========

The cluster uses Modules to manage user software environment, such as
the paths to the executable of a software package, and to the header,
library or documentation files of a library.

To see what modules are currently loaded into your environment, type::

  $ module list

To see available modules, type::

  $ module available

For more information about Modules, see http://modules.sourceforge.net/


.. _tech-summary:

System Summary
=================

+--------------------------------+------------------------------+
| Component                      |     Description              |
+================================+==============================+
| Cluster name                   | UCC2                         |
+--------------------------------+------------------------------+
| IP address                     | ucc2.idre.ucla.edu           |
+--------------------------------+------------------------------+
| Number of nodes                | 224                          |
+--------------------------------+------------------------------+
| OS                             | CentOS 6.4 GNU/Linux         |
+--------------------------------+------------------------------+
| Processor                      | Intel Xeon E5530 2.4GHz      |
+--------------------------------+------------------------------+
| CPU core per node              | 8                            |
+--------------------------------+------------------------------+
| Memory per node                | 24 GB                        |
+--------------------------------+------------------------------+
| Network                        | QDR Infiniband               |
+--------------------------------+------------------------------+
| Job scheduler                  | slurm 2.5 [1]                |
+--------------------------------+------------------------------+
| User environment management    | Modules                      |
+--------------------------------+------------------------------+

[1] http://www.schedmd.com/slurmdocs/slurm.html
