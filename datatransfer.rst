.. _data-transfer:

****************
Data Transfer
****************

Options for transferring data to or from UCC2.

Using scp
===========

To scp a file to UCC2 from your computer, use::

  $ scp /path/to/local/file <user_id>@ucc2.idre.ucla.edu:

where ``<user_id>`` is your UCC2 user ID. The command above will copy
the file to your home directory on UCC2. For more information about
scp, type ``man scp`` on UCC2.

Globus Online
==============

You may also use Global Online to transfer files to or from UCC2. See
http://hpc.ucla.edu/hoffman2/file-transfer/gol.php for more information.
